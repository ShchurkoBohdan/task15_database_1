#(вибірка з одної таблиці з простою умовою)
#1
select maker, type from Product
where type = 'Laptop';

#2
select model, ram, screen price from PC
where price > 1000;

#3
select * from Printer
where color ='y'
order by price desc;

#4
select model, speed, hd, cd, price from PC
where cd in ('12x', '24x')
order by price desc;

#5
select name, class from Ships
order by name;

#6
select * from Printer
where color != 'color' and price < 300
order by type desc;

#7
select * from PC
where speed >= 500 and price < 800
order by price desc;

#8
select model, speed from pc
where price >= 400 and price <= 600
order by hd;

#9
select model, speed, hd from pc
where hd in ('10', '20')
order by speed;

#10
select model, speed, hd, price from laptop
where screen >= 12
order by price;

#11
select model, type, price from printer
where price < 300
order by type;

#12
select model, ram, price from laptop
where ram = 64
order by screen;

#13
select model, ram, price from laptop
where ram = 64
order by hd;

#14
select model, speed, price from pc
where speed between 500 and 750
order by hd;