#(Вибірка з 2х таблиць з простою умовою)
#1
select maker,type, speed, hd from Product p
join PC
on p.model = PC.model
where hd <= 8;

#2
select distinct maker from Product p
join PC
on p.model = PC.model
where speed >= 600;

#3
select distinct maker from Product p
join PC
on p.model = PC.model
where speed <= 500;

#4
select Laptop.model, PC.model from Laptop
join PC 
on Laptop.model = PC.model
where Laptop.ram = PC.ram and Laptop.hd = PC.hd
group by Laptop.model;

#5
select country from classes
where `type` = 'bb' and country in
(select country from classes
where `type` = 'bc')
group by country;

#6
select p.model, p.maker from Product p
join PC
on p.model = PC.model
where PC.price < 600;

#7
select p.model, p.maker from Product p
join Printer
on p.model = Printer.model
where Printer.price > 300;

#8
select p.model, p.maker, PC.price from Product p
join PC
on p.model = PC.model
order by price
