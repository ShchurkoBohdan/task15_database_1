#(вибірка з одної таблиці з простою умовою)
#1
select model from pc
where model rlike '[1]{2}';

#2
select * from Outcome
where monthname(date) like 'March';

#3
select * from Outcome
where dayofmonth(date) like '14';

#4
select name from Ships
where name like 'W%n';

#5
select * from Ships
where name like '%e%e%' and
name not like '%e%e%e%';

#6
select name, launched from Ships
where name not like '%a';

#7
select * from Battles
where name rlike '[[:blank:]]+.+[a-b,d-z]$';