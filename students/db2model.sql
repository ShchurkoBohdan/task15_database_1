-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema students_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema students_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `students_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `students_db` ;

-- -----------------------------------------------------
-- Table `students_db`.`region`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`region` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `region_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students_db`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`city` (
  `id` VARCHAR(45) NOT NULL,
  `city_name` VARCHAR(45) NULL,
  `region_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_city_region1_idx` (`region_id` ASC) VISIBLE,
  CONSTRAINT `fk_city_region1`
    FOREIGN KEY (`region_id`)
    REFERENCES `students_db`.`region` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students_db`.`school`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`school` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `director` VARCHAR(45) NULL,
  `city_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_school_city1_idx` (`city_id` ASC) VISIBLE,
  CONSTRAINT `fk_school_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `students_db`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students_db`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(45) NULL,
  `group_no` VARCHAR(45) NULL,
  `year_of_entry` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students_db`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`student` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `fathers_name` VARCHAR(45) NULL,
  `general_rating` VARCHAR(45) NULL,
  `date_of_birth` VARCHAR(45) NULL,
  `date_of_entry` VARCHAR(45) NULL,
  `ticket_no` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `city_id` VARCHAR(45) NOT NULL,
  `school_id` INT NOT NULL,
  `group_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_student_city1_idx` (`city_id` ASC) VISIBLE,
  INDEX `fk_student_school1_idx` (`school_id` ASC) VISIBLE,
  INDEX `fk_student_group1_idx` (`group_id` ASC) VISIBLE,
  CONSTRAINT `fk_student_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `students_db`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_school1`
    FOREIGN KEY (`school_id`)
    REFERENCES `students_db`.`school` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_id`)
    REFERENCES `students_db`.`group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students_db`.`arrear`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`arrear` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `subject_name` VARCHAR(45) NULL,
  `student_id` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `students_db`.`student_has_arrear`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `students_db`.`student_has_arrear` (
  `student_id` INT NOT NULL,
  `arrear_id` INT NOT NULL,
  PRIMARY KEY (`student_id`, `arrear_id`),
  INDEX `fk_student_has_arrear_arrear1_idx` (`arrear_id` ASC) VISIBLE,
  INDEX `fk_student_has_arrear_student_idx` (`student_id` ASC) VISIBLE,
  CONSTRAINT `fk_student_has_arrear_student`
    FOREIGN KEY (`student_id`)
    REFERENCES `students_db`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_arrear_arrear1`
    FOREIGN KEY (`arrear_id`)
    REFERENCES `students_db`.`arrear` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
